class VacanciesController < ApplicationController
  before_action :set_vacancy, only: :show

  def index
    @vacancies = Vacancy.ordered.paginate(page: params[:page], per_page: 15)
  end

  def show
  end

  private

  def set_vacancy
    @vacancy = Vacancy.find(params[:id])
  end

  def vacancy_params
    params.fetch(:vacancy, {})
  end
end
