class ResumesController < ApplicationController
  before_action :authenticate_user!, only: [:edit, :update, :destroy]
  before_action :set_resume, only: :show

  def index
    @resumes = Resume.published
                     .ordered
                     .paginate(page: params[:page], per_page: 10)
  end

  def show
    authorize @resume, :published?
  end

  private

  def set_resume
    @resume = Resume.find(params[:id])
  end

  def resume_params
    params.require(:resume).permit(
      :title, :courses, :books, :projects, :archived, :salary,
      :experience, :skills, :remote, :relocate
    )
  end
end
