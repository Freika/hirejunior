class DashboardsController < ApplicationController
  before_action :authenticate_user!
  layout 'dashboard'

  def show
    @resumes = current_user.resumes
    @companies = current_user.companies
  end
end
