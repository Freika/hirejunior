module Dashboards
  class CompaniesController < DashboardsController
    before_action :authenticate_user!
    before_action :set_company, only: [:show, :edit, :update, :destroy]
    before_action :set_user_companies

    def index
      @companies = current_user.companies
    end

    def show
      @vacancies = @company
                    .vacancies
                    .published
                    .paginate(page: params[:page], per_page: 10)
      @archived_vacancies = @company
                              .vacancies
                              .archived
                              .paginate(page: params[:page], per_page: 10)
    end

    def new
      @company = current_user.companies.build
    end

    def edit
    end

    def create
      @company = current_user.companies.build(company_params)

      if @company.save
        redirect_to dashboards_companies_path, notice: 'Company was created.'
      else
        render :new
      end
    end

    def update
      if @company.update(company_params)
        redirect_to dashboards_companies_path, notice: 'Company was updated.'
      else
        render :edit
      end
    end

    def destroy
      @company.destroy

      redirect_to dashboards_companies_path, notice: 'Company was destroyed.'
    end

    private

    def set_company
      @company = Company.find(params[:id])
    end

    def set_user_companies
      @companies = current_user.companies
    end

    def company_params
      params.require(:company).permit(:title, :about, :url)
    end
  end
end
