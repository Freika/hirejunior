module Dashboards
  class VacanciesController < DashboardsController
    before_action :set_company
    before_action :set_vacancy, only: [
      :show, :edit, :update, :destroy, :archive, :unarchive
    ]

    def show
    end

    def new
      @vacancy = Vacancy.new
    end

    def edit
    end

    def create
      @vacancy = @company.vacancies.build(vacancy_params)

      if @vacancy.save
        redirect_to [:dashboards, @company], notice: 'Vacancy was created.'
      else
        render :new
      end
    end

    def update
      if @vacancy.update(vacancy_params)
        redirect_to [:dashboards, @company], notice: 'Vacancy was updated.'
      else
        render :edit
      end
    end

    def destroy
      @vacancy.destroy

      redirect_to dashboard_path, notice: 'Vacancy was destroyed.'
    end

    def archive
      @vacancy.archive!

      redirect_to [:dashboards, @company], notice: 'Vacancy was archived'
    end

    def unarchive
      @vacancy.unarchive!

      redirect_to [:dashboards, @company], notice: 'Vacancy was unarchived'
    end

    private

    def set_vacancy
      @vacancy = Vacancy.find(params[:id])
    end

    def set_company
      @company = Company.find(params[:company_id])
    end

    def vacancy_params
      params.require(:vacancy).permit(
        :title, :salary_from, :about, :city, :remote, :relocate, :category_id
      )
    end
  end
end
