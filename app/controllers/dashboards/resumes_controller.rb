module Dashboards
  class ResumesController < DashboardsController
    before_action :authenticate_user!
    before_action :set_resume, only: [
      :show, :edit, :update, :destroy, :archive, :unarchive
    ]
    before_action :set_user_companies

    def index
      @resumes = current_user
                  .resumes
                  .published
                  .ordered
                  .paginate(page: params[:page], per_page: 10)
      @archived_resumes = current_user
                            .resumes
                            .archived
                            .paginate(page: params[:page], per_page: 10)
    end

    def show
    end

    def new
      @resume = Resume.new
    end

    def edit
      authorize @resume
    end

    def create
      @resume = current_user.resumes.build(resume_params)

      if @resume.save
        redirect_to [:dashboards, @resume], notice: 'Resume was created.'
      else
        render :new
      end
    end

    def update
      authorize @resume

      if @resume.update(resume_params)
        redirect_to [:dashboards, @resume], notice: 'Resume was updated.'
      else
        render :edit
      end
    end

    def destroy
      authorize @resume

      @resume.destroy

      redirect_to dashboard_path, notice: 'Resume was destroyed.'
    end

    def archive
      @resume.archive!

      redirect_to [:dashboards, :resumes], notice: 'Resume was archived'
    end

    def unarchive
      @resume.unarchive!

      redirect_to [:dashboards, :resumes], notice: 'Resume was unarchived'
    end

    private

    def set_resume
      @resume = Resume.find(params[:id])
    end

    def set_user_companies
      @companies = current_user.companies
    end

    def resume_params
      params.require(:resume).permit(
        :title, :courses, :books, :projects, :archived, :experience, :skills,
        :remote, :relocate, :category_id, :salary_from, :salary_to, :currency
      )
    end
  end
end
