json.array!(@resumes) do |resume|
  json.extract! resume, :id, :title, :courses, :books, :projects, :published, :archived, :salary, :experience, :skills
  json.url resume_url(resume, format: :json)
end
