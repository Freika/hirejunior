json.extract! @resume, :id, :title, :courses, :books, :projects, :published, :archived, :salary, :experience, :skills, :created_at, :updated_at
