json.array!(@vacancies) do |vacancy|
  json.extract! vacancy, :id, :title, :salary_from, :about, :city, :remote, :relocate
  json.url vacancy_url(vacancy, format: :json)
end
