class Vacancy < ApplicationRecord
  include Scopable
  include Archivable
  belongs_to :company
  belongs_to :category
end
