# == Schema Information
#
# Table name: resumes
#
#  id          :integer          not null, primary key
#  title       :string           default(""), not null
#  courses     :text
#  books       :text
#  projects    :text
#  published   :boolean          default(FALSE)
#  archived    :boolean          default(FALSE)
#  salary      :integer
#  experience  :text
#  skills      :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  user_id     :integer
#  currency    :integer
#  salary_from :integer          default(0), not null
#  salary_to   :integer          default(0), not null
#

class Resume < ApplicationRecord
  include Scopable
  include Archivable
  belongs_to :user
  belongs_to :category

  validates :title, presence: true

  enum currency: [:rub, :usd, :eur]
end
