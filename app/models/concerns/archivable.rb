module Archivable
  extend ActiveSupport::Concern
  included do
    def archive!
      update(archived: true)
    end

    def unarchive!
      update(archived: false)
    end
  end
end
