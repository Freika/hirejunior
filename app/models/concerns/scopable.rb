module Scopable
  extend ActiveSupport::Concern
  included do
    scope :ordered, -> { order(created_at: :desc) }
    scope :archived, -> { where(archived: true) }
    scope :published, -> { where(archived: false) }
  end
end
