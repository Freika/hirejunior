class Category < ApplicationRecord
  has_many :resumes
  has_many :vacancies
end
