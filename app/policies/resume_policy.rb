class ResumePolicy < ApplicationPolicy
  def update?
    edit?
  end

  def destroy?
    owner?
  end

  def edit?
    owner?
  end

  def published?
    !record.archived?
  end

  private

  def owner?
    user == record.user
  end
end
