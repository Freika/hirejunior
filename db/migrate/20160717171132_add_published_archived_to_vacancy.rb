class AddPublishedArchivedToVacancy < ActiveRecord::Migration[5.0]
  def change
    add_column :vacancies, :published, :boolean, default: false
    add_column :vacancies, :archived, :boolean, default: false
  end
end
