class AddFieldsToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :name, :string, null: false, default: ''
    add_column :users, :surname, :string, null: false, default: ''
    add_column :users, :about, :text
    add_column :users, :github, :string
    add_column :users, :twitter, :string
    add_column :users, :city, :string
  end
end
