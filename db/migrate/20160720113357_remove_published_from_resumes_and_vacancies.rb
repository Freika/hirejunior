class RemovePublishedFromResumesAndVacancies < ActiveRecord::Migration[5.0]
  def change
    remove_column :resumes, :published, :boolean
    remove_column :vacancies, :published, :boolean
  end
end
