class AddCompanyIdToVacancy < ActiveRecord::Migration[5.0]
  def change
    add_column :vacancies, :company_id, :integer
    add_index :vacancies, :company_id
  end
end
