class AddCurrencySalaryFromSalaryToToResume < ActiveRecord::Migration[5.0]
  def change
    add_column :resumes, :currency, :integer
    add_column :resumes, :salary_from, :integer, default: 0, null: false
    add_index :resumes, :salary_from
    add_column :resumes, :salary_to, :integer, default: 0, null: false
    add_index :resumes, :salary_to
  end
end
