class RemoveSalaryFromResume < ActiveRecord::Migration[5.0]
  def change
    remove_column :resumes, :salary, :integer
  end
end
