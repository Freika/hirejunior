class AddRelocateRemoteToResume < ActiveRecord::Migration[5.0]
  def change
    add_column :resumes, :relocate, :boolean, default: false
    add_column :resumes, :remote, :boolean, default: false
  end
end
