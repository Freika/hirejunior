class CreateCompanies < ActiveRecord::Migration[5.0]
  def change
    create_table :companies do |t|
      t.string :title
      t.text :about
      t.string :url
      t.integer :user_id

      t.timestamps
    end
    add_index :companies, :user_id
  end
end
