class AddCategoryIdToVacancyAndResume < ActiveRecord::Migration[5.0]
  def change
    add_column :vacancies, :category_id, :integer
    add_index :vacancies, :category_id

    add_column :resumes, :category_id, :integer
    add_index :resumes, :category_id
  end
end
