class CreateVacancies < ActiveRecord::Migration[5.0]
  def change
    create_table :vacancies do |t|
      t.string :title, null: false, default: ''
      t.integer :salary_from
      t.text :about
      t.string :city
      t.boolean :remote, defauld: false
      t.boolean :relocate, defauld: false

      t.timestamps
    end
    add_index :vacancies, :salary_from
  end
end
