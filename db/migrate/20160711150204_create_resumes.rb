class CreateResumes < ActiveRecord::Migration[5.0]
  def change
    create_table :resumes do |t|
      t.string :title, default: '', null: false
      t.text :courses
      t.text :books
      t.text :projects
      t.boolean :published, default: false
      t.boolean :archived, default: false
      t.integer :salary
      t.text :experience
      t.text :skills

      t.timestamps
    end
  end
end
