# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

user = User.create(
  email: 'test@test.ru',
  password: '00000000',
  password_confirmation: '00000000'
)
p 'User created'

categories = %w(
  Ruby Python PHP Javascript Fullstack Desigh Java C++ .NET Android iOS
)
categories.each do |category|
  Category.create(title: category)
end
p 'Categories created'

30.times do
  FactoryGirl.create(:resume, user: user)
end
p 'Resumes created'
