Rails.application.routes.draw do
  namespace :dashboards, path: 'dashboard' do
    resources :companies do
      resources :vacancies do
        put :archive, on: :member
        put :unarchive, on: :member
      end
    end
    resources :resumes do
      put :archive, on: :member
      put :unarchive, on: :member
    end
  end
  resource :dashboard
  resources :vacancies

  resources :resumes
  devise_for :users
  root to: 'pages#home'
end
