require 'rails_helper'

describe Vacancy, type: :model do
  it { should belong_to :company }
  it { should belong_to :category }

  describe 'model methods' do
    let(:user) { create(:user) }
    let(:company) { create(:company, user: user) }
    let(:vacancy) { create(:published_vacancy, company: company) }
    it '#archive!' do
      vacancy.archive!

      expect(vacancy.archived).to be_truthy
    end
  end
end
