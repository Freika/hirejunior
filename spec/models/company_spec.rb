require 'rails_helper'

describe Company, type: :model do
  it { should belong_to :user }
  it { should have_many :vacancies }
end
