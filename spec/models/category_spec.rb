require 'rails_helper'

describe Category, type: :model do
  it { should have_many :resumes }
  it { should have_many :vacancies }
end
