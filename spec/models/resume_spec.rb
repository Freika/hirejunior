# == Schema Information
#
# Table name: resumes
#
#  id          :integer          not null, primary key
#  title       :string           default(""), not null
#  courses     :text
#  books       :text
#  projects    :text
#  published   :boolean          default(FALSE)
#  archived    :boolean          default(FALSE)
#  salary      :integer
#  experience  :text
#  skills      :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  user_id     :integer
#  currency    :integer
#  salary_from :integer          default(0), not null
#  salary_to   :integer          default(0), not null
#

require 'rails_helper'

describe Resume do
  it { should belong_to(:user) }
  it { should belong_to(:category) }

  describe 'model tests' do
    let(:user) { create(:user) }
    let(:category) { create(:category) }
    let(:resume) { create(:published_resume, user: user, category: category) }

    it '#archive!' do
      resume.archive!

      expect(resume.archived).to be_truthy
    end
  end
end
