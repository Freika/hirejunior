describe Dashboards::CompaniesController, type: :controller do
  let!(:user) { create(:user) }
  let(:valid_attributes) { attributes_for(:company) }

  let(:invalid_attributes) { skip }

  before(:each) do
    sign_in(user, scope: :user)
    valid_attributes[:user_id] = user.id
  end

  describe 'GET #index' do
    it 'assigns all companies as @companies' do
      company = Company.create! valid_attributes
      get :index
      expect(assigns(:companies)).to eq([company])
    end
  end

  describe 'GET #show' do
    it 'assigns the requested company as @company' do
      company = Company.create! valid_attributes
      get :show, params: { id: company.id }
      expect(assigns(:company)).to eq(company)
    end

    it 'assigns the requested company vacancies as @vacancies' do
      company = Company.create! valid_attributes
      get :show, params: { id: company.id }
      expect(assigns(:vacancies)).to eq(company.vacancies)
    end
  end

  describe 'GET #new' do
    it 'assigns a new company as @company' do
      get :new
      expect(assigns(:company)).to be_a_new(Company)
    end
  end

  describe 'GET #edit' do
    it 'assigns the requested company as @company' do
      company = Company.create! valid_attributes
      get :edit, params: { id: company.id }
      expect(assigns(:company)).to eq(company)
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new Company' do
        expect do
          post :create, params: { company: valid_attributes }
        end.to change(Company, :count).by(1)
      end

      it 'assigns a newly created company as @company' do
        post :create, params: { company: valid_attributes }
        expect(assigns(:company)).to be_a(Company)
        expect(assigns(:company)).to be_persisted
      end

      it 'redirects to the created company' do
        post :create, params: { company: valid_attributes }
        expect(response).to redirect_to(dashboards_companies_path)
      end
    end

    context 'with invalid params' do
      it 'assigns a newly created but unsaved company as @company' do
        post :create, params: { company: invalid_attributes }
        expect(assigns(:company)).to be_a_new(Company)
      end

      it 're-renders the \'new\' template' do
        post :create, params: { company: invalid_attributes }
        expect(response).to render_template('new')
      end
    end
  end

  describe 'PUT #update' do
    context 'with valid params' do
      let(:new_attributes) { skip }

      it 'updates the requested company' do
        company = Company.create! valid_attributes
        put :update, params: { id: company.id, company: new_attributes }
        company.reload
        skip('Add assertions for updated state')
      end

      it 'assigns the requested company as @company' do
        company = Company.create! valid_attributes
        put :update, params: { id: company.id, company: valid_attributes }
        expect(assigns(:company)).to eq(company)
      end

      it 'redirects to the company' do
        company = Company.create! valid_attributes
        put :update, params: { id: company.id, company: valid_attributes }
        expect(response).to redirect_to(dashboards_companies_path)
      end
    end

    context 'with invalid params' do
      it 'assigns the company as @company' do
        company = Company.create! valid_attributes
        put :update, params: { id: company.id, company: invalid_attributes }
        expect(assigns(:company)).to eq(company)
      end

      it 're-renders the \'edit\' template' do
        company = Company.create! valid_attributes
        put :update, params: { id: company.id, company: invalid_attributes }
        expect(response).to render_template('edit')
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested company' do
      company = Company.create! valid_attributes
      expect do
        delete :destroy, params: { id: company.id }
      end.to change(Company, :count).by(-1)
    end

    it 'redirects to the companies list' do
      company = Company.create! valid_attributes
      delete :destroy, params: { id: company.id }
      expect(response).to redirect_to(dashboards_companies_path)
    end
  end
end
