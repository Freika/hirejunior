describe Dashboards::VacanciesController, type: :controller do

  let(:valid_vacancy) { attributes_for(:vacancy) }
  let(:user) { create(:user) }
  let(:company) { create(:company, user: user) }
  let!(:category) { create(:category) }

  let(:invalid_vacancy) { skip }
  before(:each) do
    sign_in(user, scope: :user)
    valid_vacancy[:company_id] = company.id
    valid_vacancy[:category_id] = category.id
  end

  describe 'GET #show' do
    it 'assigns the requested vacancy as @vacancy' do
      vacancy = Vacancy.create! valid_vacancy
      get :show, params: { id: vacancy.id, company_id: company.id }
      expect(assigns(:vacancy)).to eq(vacancy)
    end
  end

  describe 'GET #new' do
    it 'assigns a new vacancy as @vacancy' do
      get :new, params: { company_id: company.id }
      expect(assigns(:vacancy)).to be_a_new(Vacancy)
    end
  end

  describe 'GET #edit' do
    it 'assigns the requested vacancy as @vacancy' do
      vacancy = Vacancy.create! valid_vacancy
      get :edit, params: { id: vacancy.id, company_id: company.id }
      expect(assigns(:vacancy)).to eq(vacancy)
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new Vacancy' do
        expect do
          post :create, params: { vacancy: valid_vacancy, company_id: company.id }
        end.to change(Vacancy, :count).by(1)
      end

      it 'assigns a newly created vacancy as @vacancy' do
        post :create, params: { vacancy: valid_vacancy, company_id: company.id }
        expect(assigns(:vacancy)).to be_a(Vacancy)
        expect(assigns(:vacancy)).to be_persisted
      end

      it 'redirects to the created vacancy' do
        post :create, params: { vacancy: valid_vacancy, company_id: company.id }
        expect(response).to redirect_to(dashboards_company_path(company))
      end
    end

    context 'with invalid params' do
      it 'assigns a newly created but unsaved vacancy as @vacancy' do
        post :create, params: { vacancy: invalid_vacancy }
        expect(assigns(:vacancy)).to be_a_new(Vacancy)
      end

      it 're-renders the \'new\' template' do
        post :create, params: { vacancy: invalid_vacancy }
        expect(response).to render_template('new')
      end
    end
  end

  describe 'PUT #update' do
    context 'with valid params' do
      let(:new_attributes) { skip }

      it 'updates the requested vacancy' do
        vacancy = Vacancy.create! valid_vacancy
        put :update, params: { id: vacancy.id, vacancy: new_attributes }
        vacancy.reload
        skip('Add assertions for updated state')
      end

      it 'assigns the requested vacancy as @vacancy' do
        vacancy = Vacancy.create! valid_vacancy
        put :update, params: { id: vacancy.id, vacancy: valid_vacancy, company_id: company.id }
        expect(assigns(:vacancy)).to eq(vacancy)
      end

      it 'redirects to the vacancy' do
        vacancy = Vacancy.create! valid_vacancy
        put :update, params: { id: vacancy.id, vacancy: valid_vacancy, company_id: company.id }
        expect(response).to redirect_to(dashboards_company_path(company))
      end
    end

    context 'with invalid params' do
      it 'assigns the vacancy as @vacancy' do
        vacancy = Vacancy.create! valid_vacancy
        put :update, params: { id: vacancy.id, vacancy: invalid_vacancy }
        expect(assigns(:vacancy)).to eq(vacancy)
      end

      it 're-renders the \'edit\' template' do
        vacancy = Vacancy.create! valid_vacancy
        put :update, params: { id: vacancy.id, vacancy: invalid_vacancy }
        expect(response).to render_template('edit')
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested vacancy' do
      vacancy = Vacancy.create! valid_vacancy
      expect do
        delete :destroy, params: { id: vacancy.id, company_id: company.id }
      end.to change(Vacancy, :count).by(-1)
    end

    it 'redirects to the vacancies list' do
      vacancy = Vacancy.create! valid_vacancy
      delete :destroy, params: { id: vacancy.id, company_id: company.id }
      expect(response).to redirect_to(dashboard_path)
    end
  end

  describe 'archivation' do
    let!(:user) { create(:user) }
    let!(:company) { create(:company, user: user) }
    let!(:vacancy) { create(:published_vacancy, company: company) }

    describe 'PUT #archive' do
      it 'archives vacancy' do
        put :archive, params: { id: vacancy.id, company_id: company.id }
        expect(vacancy.reload.archived).to be_truthy
      end
    end

    describe 'PUT #unarchive' do
      it 'unarchives vacancy' do
        put :unarchive, params: { id: vacancy.id, company_id: company.id }
        expect(vacancy.reload.archived).to be_falsey
      end
    end
  end
end
