describe Dashboards::ResumesController, type: :controller do
  let(:valid_attributes) { attributes_for(:resume) }

  let(:invalid_attributes) { skip }

  let!(:user) { create(:user) }
  let!(:category) { create(:category) }
  let!(:visitor) { create(:user) }
  let!(:resume) { create(:published_resume, user: user, category: category) }
  before(:each) do
    sign_in(user, scope: :user)
    valid_attributes[:category_id] = category.id
  end

  describe 'GET #index' do
    it 'assigns all resumes as @resumes' do
      get :index
      expect(assigns(:resumes)).to eq([resume])
    end
  end

  describe 'GET #show' do
    it 'assigns the requested resume as @resume' do
      get :show, params: { id: resume.id }
      expect(assigns(:resume)).to eq(resume)
    end
  end

  describe 'GET #new' do
    it 'assigns a new resume as @resume' do
      get :new
      expect(assigns(:resume)).to be_a_new(Resume)
    end
  end

  describe 'GET #edit' do
    it 'assigns the requested resume as @resume' do
      get :edit, params: { id: resume.id }
      expect(assigns(:resume)).to eq(resume)
    end

    it 'can\'t be updated by another user' do
      sign_in(visitor, scope: :user)

      get :edit, params: { id: resume.id }

      expect(response.status).to eq(302)
      expect(flash[:alert]).to eq(
        'You are not authorized to perform this action.'
      )
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new Resume' do
        expect do
          post :create, params: { resume: valid_attributes }
        end.to change(Resume, :count).by(1)
      end

      it 'assigns a newly created resume as @resume' do
        post :create, params: { resume: valid_attributes }
        expect(assigns(:resume)).to be_a(Resume)
        expect(assigns(:resume)).to be_persisted
      end

      it 'redirects to the created resume' do
        post :create, params: { resume: valid_attributes }
        expect(response).to redirect_to(dashboards_resume_path(Resume.last))
      end
    end

    context 'with invalid params' do
      it 'assigns a newly created but unsaved resume as @resume' do
        post :create, params: { resume: invalid_attributes }
        expect(assigns(:resume)).to be_a_new(Resume)
      end

      it 're-renders the \'new\' template' do
        post :create, params: { resume: invalid_attributes }
        expect(response).to render_template('new')
      end
    end
  end

  describe 'PUT #update' do
    context 'with valid params' do
      let(:new_attributes) { skip }

      it 'updates the requested resume' do
        put :update, params: { id: resume.id, resume: new_attributes }
        resume.reload
        skip('Add assertions for updated state')
      end

      it 'assigns the requested resume as @resume' do
        put :update, params: { id: resume.id, resume: valid_attributes }
        expect(assigns(:resume)).to eq(resume)
      end

      it 'redirects to the resume' do
        put :update, params: { id: resume.id, resume: valid_attributes }
        expect(response).to redirect_to(dashboards_resume_path(resume))
      end

      it 'can\'t be updated by another user' do
        sign_in(visitor, scope: :user)

        put :update, params: { id: resume.id, resume: valid_attributes }

        expect(response.status).to eq(302)
        expect(flash[:alert]).to eq(
          'You are not authorized to perform this action.'
        )
      end
    end

    context 'with invalid params' do
      it 'assigns the resume as @resume' do
        put :update, params: { id: resume.id, resume: invalid_attributes }
        expect(assigns(:resume)).to eq(resume)
      end

      it 're-renders the \'edit\' template' do
        put :update, params: { id: resume.id, resume: invalid_attributes }
        expect(response).to render_template('edit')
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested resume' do
      expect do
        delete :destroy, params: { id: resume.id }
      end.to change(Resume, :count).by(-1)
    end

    it 'redirects to the resumes list' do
      delete :destroy, params: { id: resume.id }
      expect(response).to redirect_to(dashboard_path)
    end

    it 'can\'t be deleted by another user' do
      sign_in(visitor, scope: :user)

      delete :destroy, params: { id: resume.id }

      expect(response.status).to eq(302)
      expect(flash[:alert]).to eq(
        'You are not authorized to perform this action.'
      )
    end
  end

  describe 'PUT #archive' do
    it 'archives resume' do
      put :archive, params: { id: resume.id }
      expect(resume.reload.archived).to be_truthy
    end
  end

  describe 'PUT #unarchive' do
    it 'unarchives resume' do
      put :unarchive, params: { id: resume.id }
      expect(resume.reload.archived).to be_falsey
    end
  end
end
