describe DashboardsController, type: :controller do
  let!(:user) { create(:user) }
  let!(:category) { create(:category) }
  let!(:resume) { create(:published_resume, user: user, category: category) }
  before(:each) do
    sign_in(user, scope: :user)
  end

  describe 'GET #show' do
    before(:each) do
      get :show
    end

    it 'returns http success' do
      expect(response).to have_http_status(:success)
    end

    it 'contains all user resumes' do
      expect(assigns(:resumes)).to eq(user.resumes)
    end

    it 'contains all user companies' do
      expect(assigns(:companies)).to eq(user.companies)
    end
  end
end
