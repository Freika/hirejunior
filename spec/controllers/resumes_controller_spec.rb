describe ResumesController, type: :controller do
  let(:valid_attributes) { attributes_for(:resume) }
  let(:invalid_attributes) { skip }
  let!(:user) { create(:user) }
  let!(:visitor) { create(:user) }
  let!(:category) { create(:category) }
  let!(:resume) { create(:published_resume, user: user, category: category) }
  before(:each) do
    sign_in(user, scope: :user)
  end

  describe 'GET #index' do
    it 'assigns all resumes as @resumes' do
      get :index
      expect(assigns(:resumes)).to eq([resume])
    end
  end

  describe 'GET #show' do
    it 'assigns the requested resume as @resume' do
      get :show, params: { id: resume.to_param }
      expect(assigns(:resume)).to eq(resume)
    end
  end
end
