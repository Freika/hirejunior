describe 'User' do
  let(:user) { create(:user) }

  context 'profile' do
    before(:each) do
      login_as user
    end

    it 'updates all fields' do
      visit edit_user_registration_path

      name = Faker::Name.first_name
      surname = Faker::Name.last_name
      about = Faker::Name.title
      github = Faker::Internet.user_name
      twitter = Faker::Internet.user_name
      city = Faker::Address.city

      fill_in 'user_name', with: name
      fill_in 'user_surname', with: surname
      fill_in 'user_about', with: about
      fill_in 'user_github', with: github
      fill_in 'user_twitter', with: twitter
      fill_in 'user_city', with: city
      fill_in 'user_current_password', with: user.password

      click_on 'Сохранить'

      visit edit_user_registration_path

      user.reload

      expect(user.name).to eq(name)
      expect(user.surname).to eq(surname)
      expect(user.about).to eq(about)
      expect(user.github).to eq(github)
      expect(user.twitter).to eq(twitter)
      expect(user.city).to eq(city)
    end
  end
end
