describe 'Vacancy' do
  let!(:user) { create(:user) }
  let!(:company) { create(:company, user: user) }
  let!(:published_vacancy) { create(:published_vacancy, company: company) }
  let!(:archived_vacancy) { create(:archived_vacancy, company: company) }

  before(:each) do
    login_as user
  end

  it 'can be #archived' do
    visit dashboards_company_path(company)

    click_on 'Archive'

    within '#archived_vacancies' do
      expect(page).to have_link(published_vacancy.title)
    end
  end

  it 'can be unarchived' do
    visit dashboards_company_path(company)

    within '#archived_vacancies' do
      click_on 'Unarchive'
    end

    within '#published_vacancies' do
      expect(page).to have_link(archived_vacancy.title)
    end
  end
end
