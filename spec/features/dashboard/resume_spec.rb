describe 'Resume' do
  let!(:user) { create(:user) }
  let!(:category) { create(:category) }
  let!(:resume) { create(:published_resume, user: user, category: category) }

  before(:each) do
    login_as user
  end

  it 'allows user to delete his resume' do
    visit dashboards_resumes_path

    within "#resume-#{resume.id}" do
      click_on 'Delete'
    end

    expect(page).to have_content('Resume was destroyed.')
  end
end
