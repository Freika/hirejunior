describe 'Resume' do
  let!(:user) { create(:user) }
  let!(:category) { create(:category) }

  before(:each) do
    login_as user
  end

  context 'creating' do
    before(:each) do
      visit new_dashboards_resume_path

      fill_in 'resume_title', with: Faker::Name.title
      fill_in 'resume_salary_from', with: 100
      fill_in 'resume_salary_to', with: 500
      select 'rub', from: 'resume_currency'
      fill_in 'resume_experience', with: 'Lorem ipsum dolor sit.'
      fill_in 'resume_skills', with: 'Lorem ipsum dolor.'
      fill_in 'resume_courses', with: 'Lorem ipsum dolor.'
      fill_in 'resume_books', with: 'The Pickaxe'
      fill_in 'resume_projects', with: 'I created Google!'
      check 'resume_relocate'
      check 'resume_remote'
      select category.title, from: 'resume_category_id'

      click_on 'Опубликовать'
    end

    it 'can be created by existing user' do
      expect(page).to have_content('Resume was created.')
    end

    it 'can be #archived' do
      resume = Resume.first

      visit dashboards_resumes_path

      click_on 'Archive'

      visit dashboards_resumes_path
      within '#archived_resumes' do
        expect(page).to have_link(resume.title)
      end
    end

    it 'can be unarchived' do
      resume = Resume.first
      resume.archive!

      visit dashboards_resumes_path

      within '#archived_resumes' do
        click_on 'Unarchive'
      end

      within '#published_resumes' do
        expect(page).to have_link(resume.title)
      end
    end
  end

  context 'updating' do
    let(:visitor) { create(:user) }
    let(:resume) { create(:published_resume, user: user, category: category) }

    it 'owner can see update link' do
      login_as user

      visit resume_path(resume)

      expect(page).to have_content('Edit')
    end

    it 'another user can not see update link' do
      login_as visitor

      visit resume_path(resume)

      expect(page).to_not have_content('Edit')
    end
  end
end
