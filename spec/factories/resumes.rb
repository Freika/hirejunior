# == Schema Information
#
# Table name: resumes
#
#  id          :integer          not null, primary key
#  title       :string           default(""), not null
#  courses     :text
#  books       :text
#  projects    :text
#  published   :boolean          default(FALSE)
#  archived    :boolean          default(FALSE)
#  salary      :integer
#  experience  :text
#  skills      :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  user_id     :integer
#  currency    :integer
#  salary_from :integer          default(0), not null
#  salary_to   :integer          default(0), not null
#

FactoryGirl.define do
  factory :resume do
    title { Faker::Name.title }
    courses { Faker::Educator.course }
    books { Faker::Book.title }
    projects 'http://google.com'
    salary_from { rand(100..500) }
    salary_to { rand(500..1000) }
    experience { Faker::Lorem.paragraph }
    skills { Faker::Lorem.paragraph }

    trait :published do
      archived false
    end

    trait :archived do
      archived true
    end

    factory :published_resume, traits: [:published]
    factory :archived_resume, traits: [:archived]
  end
end
