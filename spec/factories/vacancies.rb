FactoryGirl.define do
  factory :vacancy do
    title { Faker::Company.name }
    salary_from { rand(100.5000) }
    about { Faker::Lorem.paragraph }
    city 'Moscow'
    remote false
    relocate false
    company
    category

    trait :published do
      archived false
    end

    trait :archived do
      archived true
    end

    factory :published_vacancy, traits: [:published]
    factory :archived_vacancy, traits: [:archived]
  end
end
