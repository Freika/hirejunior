FactoryGirl.define do
  factory :category do
    title { Faker::Company.name }
    about { Faker::Lorem.paragraph }
  end
end
