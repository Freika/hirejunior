FactoryGirl.define do
  factory :company do
    title { Faker::Company.name }
    about { Faker::Lorem.paragraph }
    url { Faker::Internet.domain_name }
    user
  end
end
